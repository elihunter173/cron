#!/bin/bash

# This script forcibly pulls, creates a package list for many known package managers
# using the name <package-manager-name>-packages.txt, and commits those package lists,
# and then pushes all commits.

if [ $# != 1 ]; then
    echo "USAGE: package-backup.sh REPO_DIR"
    echo "  Where REPO_DIR is the local directory containing the git repository"
    echo "        that should have the package lists."
    exit
fi

repo=$1
pacman_list=$repo/pacman-packages.txt
pip_list=$repo/pip-packages.txt
npm_list=$repo/npm-packages.json
gem_list=$repo/gem-packages.txt

git -C $repo pull --force # This is to help prevent failure if I change the repo structure and forget to pull here

# If the program is installed, it will successfully print out the version.
pacman --version && \
    pacman -Qq >$pacman_list && \
    git -C $repo add $pacman_list && \
    git -C $repo commit -m "Automatic pacman package list update"
pip --version && \
    pip list --format freeze | sed 's/==.\+$//' >$pip_list && \
    git -C $repo add $pip_list && \
    git -C $repo commit -m "Automatic pip package list update"
npm --version && \
    npm list -g --depth=0 --json >$npm_list && \
    git -C $repo add $npm_list && \
    git -C $repo commit -m "Automatic npm package list update"
gem --version && \
    gem list | gem list | sed 's/^/gem "/' | sed 's/\s(.\+/"/' >$gem_list && \
    git -C $repo add $gem_list && \
    git -C $repo commit -m "Automatic gem package list update"

git -C $repo push
