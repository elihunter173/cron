#!/bin/bash

# This script syncs a local directory to rclone's eos profile for the current user.
# The directory is synced at the home directory using the basename of the local directory.

if [ $# != 1 ]; then
    echo "USAGE:"
    echo "  eos-sync.sh COPY_DIR"
    echo "    Where COPY_DIR is the local directory to be synced from."
    exit
fi

# This is normally run in the background by cron, so I don't bother discarding output

# Set up Variables
copy_path=$1
copy_basepath=${copy_path##*/}
copy_dirpath=${copy_path%$copy_basepath}

# -vv (very verbose) flag is for debugging
# --delete deletes old files
# --transfers describes how many files can be created in parrallel
rclone sync -vv --delete --transfers 16 \
    $copy_path eos:$copy_basepath
