#!/bin/bash

# This script copies a compressed copy of a local directory and uploads it using
# rclone's ncsu-drive profile that I set up. It uploads it under a
# /backup/<basename> dir in that drive with a name of the date in the form MM-DD-YYYY.
# ncsu-drive must be set up prior to the usage of this script.

if [ $# != 1 ] && [ $# != 2 ]; then
    echo "USAGE:"
    echo "  ncsu-rclone-backup.sh COPY_DIR [ZIP_FILE]"
    echo "    Where COPY_DIR is the local directory to be copied from."
    echo "      and ZIP_FILE is the path to the zip file."
    exit
fi

# This is normally run in the background by cron, so I don't bother discarding output

# Set up Variables
copy_path=$1

copy_basepath=${copy_path##*/}
copy_dirpath=${copy_path%$copy_basepath}

# If $2 is empty, set up the default path, otherwise, use the given path
if [ -z $2 ]; then
    zip_path=$copy_dirpath/$(date +"%m-%d-%Y").zip
else
    zip_path=$2
fi

# Compress the file (ignoring .git) for faster upload
zip -r $zip_path $copy_path -x "*.git*"

# -vv (very verbose) flag is for debugging
# --tpslimit is so Drive doesn't generate 403 errors.
# --transfers describes how many files can be created in parrallel
# --drive-chunk-size determines how large the file in RAM can be before upload
rclone copy -vv --tpslimit 10 --transfers 16 --drive-chunk-size 128M \
    $zip_path ncsu-drive:backup/$copy_basepath
# NOTE:
# I have my own client ID set up for this

echo "Removing temporary zip file..."
rm $zip_path
