#!/bin/bash

# This script forcibly pulls, adds and commits everything, and then pushes
# a repository to whatever the current branch and set URL for that repo is.
# NOTE: This should be used exclusively as CYA.

if [ $# != 1 ]; then
    echo "USAGE: git-repo-sync.sh REPO_DIR"
    echo "  Where REPO_DIR is the local directory containing the git repository."
    exit
fi

repo=$1

git -C $repo pull --force # This is to help prevent failure if I change the repo structure and forget to pull here

git -C $repo add --all && git -C $repo commit -m "Automatic sync"

git -C $repo push
