#!/bin/bash

# This script forcibly pulls, creates a crontab of the current user at
# the repository's root named `crontab.txt`, adds that newly created/overwritten
# file to be commited, commits it, and then pushes a repository.

if [ $# != 1 ]; then
    echo "USAGE: cron-backup.sh REPO_DIR"
    echo "  Where REPO_DIR is the local directory containing the git repository"
    echo "        that should have the crontab."
    exit
fi

repo=$1
crontab=$repo/crontab.txt

git -C $repo pull --force # This is to help prevent failure if I change the repo structure and forget to pull here

crontab -l >$crontab && git -C $repo add --all && git -C $repo commit -m "Automatic cron scripts update"

git -C $repo push
